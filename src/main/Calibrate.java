package main;

import behaviors.BehaviorCalibrate;
import behaviors.BehaviorEndRun;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class Calibrate {
	public static void main(String[] args) {
		Behavior[] bArray = {
				new BehaviorCalibrate()
		};
		Arbitrator arb = new Arbitrator(bArray);
		arb.go();
	}
}
