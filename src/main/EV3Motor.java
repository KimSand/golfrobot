package main;

import lejos.hardware.BrickFinder;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;

public class EV3Motor {
	
    /**
     * Motor A.
     */
    public static final EV3MediumRegulatedMotor A = new EV3MediumRegulatedMotor(BrickFinder.getDefault().getPort("A"));
    /**
     * Motor B.
     */
    public static final EV3LargeRegulatedMotor B = new EV3LargeRegulatedMotor(BrickFinder.getDefault().getPort("B"));
    /**
     * Motor C.
     */
    public static final EV3LargeRegulatedMotor C = new EV3LargeRegulatedMotor(BrickFinder.getDefault().getPort("C"));
    
    /**
     * Motor D.
     */
    public static final EV3MediumRegulatedMotor D = new EV3MediumRegulatedMotor(BrickFinder.getDefault().getPort("D"));
    
    private EV3Motor() {
    	// Motor class cannot be instantiated
    }
}
