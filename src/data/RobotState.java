package data;

import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.GyroscopeAdapter;
import lejos.robotics.RangeFinderAdapter;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.mapping.LineMap;
import lejos.robotics.navigation.CompassPilot;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.PathFinder;
import lejos.robotics.pathfinding.ShortestPathFinder;
import lejos.utility.GyroDirectionFinder;
import lejos.utility.Timer;


@SuppressWarnings("deprecation")
public class RobotState {
	private static RobotState instance = null;
	
	private boolean debug = false;
	private boolean soundEnabled = false;

	/* **** BOOLEAN STATE VARIABLES ***** */
	private boolean activeConnection;
	private boolean connectionFailure;
	private boolean ballsDelivered;
	private boolean ballInDangerZone;
	private boolean ballInCorner;
	private boolean ballInCross;
	private boolean ballLocationReceived;
	private boolean crossReceived;
	private boolean moveToGoal;
	private boolean poseReceived;
	private boolean poseFailure;
	private boolean updatedLinemap;
	private boolean readyToMoveToDestination;
	private boolean readyToStart;
	private boolean requestBall;
	private boolean poseUptoDate;
	private boolean poseRequested;
	private boolean haveLinemap;
	private boolean noMoreBalls;
	private boolean collectInDangerZone;
	private boolean deliverBalls;
	private boolean updateCrossPosition;
	private boolean updateGoalLocation;
	private boolean timeIsUp;


	/* **** DATA VARIABLES **** */
	private Waypoint ballLocation = null;
	private Waypoint currentDestination = null;
	private Waypoint crossCenter;
	private double crossDangerZone; // tallet 5 er halvdelen af krydset højde
	private double dangerZone = 21.0;
	private Waypoint goalLocation = null; //TODO
	private GyroDirectionFinder directionFinder;
	private double linearAcceleration = 20;
	private double linearSpeed = 20.0;
	private LineMap lineMap = null;
	private double max_X = 166.27; //TODO: Skal beregnes! !!!!!!!!!!!!
	private double max_Y = 120.65; //TODO: Skal beregnes! !!!!!!!!!!!!
	private RegulatedMotor motorA = Motor.A;
	private RegulatedMotor motorB = Motor.B;
	private RegulatedMotor motorC = Motor.C;
	private RegulatedMotor motorD = Motor.D;
	private EV3IRSensor irSensor = new EV3IRSensor(SensorPort.S2);
	private SampleProvider sampleProvider = irSensor.getDistanceMode();
	private Timer eventTimer = null;

	private Navigator navigator;
	private ShortestPathFinder pathFinder;
	private DifferentialPilot pilot;
	private Pose receivedPose;
	private long timeStampStart;
	private long timeStampStop;
	private final double ROBOT_NOSE_LENGTH = 18.0;
	private double trackwidth = 13.5;
	private double wheelDiameter = 4.14;
	private GyroscopeAdapter gyro;
	
	private RobotState() {
		stateDefault();
		gyro = new GyroscopeAdapter(
				new EV3GyroSensor(SensorPort.S1), 0);
		
		
		directionFinder = new GyroDirectionFinder(gyro);
//		pilot = new CompassPilot(directionFinder, 
//				(float) wheelDiameter, 
//				(float) trackwidth, 
//				motorC, 
//				motorB);
		pilot = new DifferentialPilot(wheelDiameter, trackwidth, motorC, motorB);
		pilot.setLinearSpeed(linearSpeed);
		pilot.setLinearAcceleration(linearAcceleration);
		crossDangerZone = 5.0 + dangerZone;
		navigator = new Navigator(pilot);
	}

	private RangeFinderAdapter rf = new RangeFinderAdapter(sampleProvider);
	public double getLinearAcceleration() {
		return linearAcceleration;
	}

	public double getLinearSpeed() {
		return linearSpeed;
	}

	public void stateDefault() {
		readyToStart = false;
		activeConnection = false;
		ballInDangerZone = false;
		ballInCorner = false;
		ballLocationReceived = false;
		ballInCross = false;
		crossReceived = false;
		connectionFailure = false;
		moveToGoal = false;
		poseReceived = false;
		poseFailure = false;
		updatedLinemap = false;
		readyToMoveToDestination = false;
		requestBall = true;
		poseUptoDate = false;
		poseRequested = false;
		haveLinemap = false;
		noMoreBalls = false;
		collectInDangerZone = false;
		deliverBalls = false;
		updateCrossPosition = true;
		updateGoalLocation = true;
		ballsDelivered = false;
		timeIsUp = false;
	}

	public void stateBallReceived() {
		requestBall = false;
		noMoreBalls = false;
		ballsDelivered = false;
		ballLocationReceived = true;
	}

	public void statePoseFailure() {
		poseFailure = true;
	}
	
	public void stateRequestPose() {
		poseRequested = false;
		poseUptoDate = false;
		poseReceived = false;
		readyToMoveToDestination = false;
	}

	public void stateRequestBall() {
		requestBall = true;
		noMoreBalls = false;
		ballInDangerZone = false;
		ballInCorner = false;
		ballInCross = false;
		ballLocationReceived = false;
		readyToMoveToDestination = false;
		collectInDangerZone = false;
	}

	public void stateStartRecvLinemap() {
		updatedLinemap = false;
		haveLinemap = false;
		updateCrossPosition = true;
		updateGoalLocation = true;
	}

	public void stateBallsDelivered() {
		setDeliverBalls(false);
		setBallsDelivered(true);
		stateRequestBall();
	}
	
	public void stateExit() {
		ballsDelivered = true;
		noMoreBalls = true;
	}
	/*** GETTERS AND SETTERS ***/
	public Waypoint getBallLocation() {
		ballLocationReceived = false;
		return ballLocation;
	}

	public Waypoint getCrossCenter() {
		return crossCenter;
	}

	public double getCrossDangerZone() {
		return crossDangerZone;
	}

	public Waypoint getCurrentDestination() {
		return currentDestination;
	}

	public double getDangerZone() {
		return dangerZone;
	}

	public Waypoint getDeliverLocation() {
		return new Waypoint(0f,0f);
	}

	public Waypoint getGoalLocation() {
		return goalLocation;
	}
	
	public GyroscopeAdapter getGyro() {
		return gyro;
	}

	public EV3IRSensor getIrSensor() {
		return irSensor;
	}

	public RangeFinderAdapter getRf() {
		return rf;
	}

	public GyroDirectionFinder getDirectionFinder() {
		return directionFinder;
	}
	
	public static RobotState getInstance() {
		if(instance==null) {
			instance = new RobotState();
		}
		return instance;
	}

	public LineMap getLineMap() {
		updatedLinemap = false;
		return lineMap;
	}

	public double getMax_X() {
		return max_X;
	}

	public double getMax_Y() {
		return max_Y;
	}

	public RegulatedMotor getMotorA() {
		return motorA;
	}

	public RegulatedMotor getMotorB() {
		return motorB;
	}

	public RegulatedMotor getMotorC() {
		return motorC;
	}

	public RegulatedMotor getMotorD() {
		return motorD;
	}

	public Navigator getNavigator() {
		return navigator;
	}

	public PathFinder getPathFinder() {
		return pathFinder;
	}

	public DifferentialPilot getPilot() {
		return pilot;
	}

	public double getROBOT_NOSE_LENGTH() {
		return ROBOT_NOSE_LENGTH;
	}
	
	public Pose getReceivedPose() {
		poseReceived = false;
		return receivedPose;
	}

	public double getTrackwidth() {
		return trackwidth;
	}

	public double getWheelDiameter() {
		return wheelDiameter;
	}

	public boolean isDebug() {
		return debug;
	}

	public boolean isConnectionFailure() {
		return connectionFailure;
	}

	public boolean isCrossReceived() {
		return crossReceived;
	}

	public boolean isBallInCross() {
		return ballInCross;
	}
	
	public boolean isBallsDelivered() {
		return ballsDelivered;
	}
	
	public boolean isSoundEnabled() {
		return soundEnabled;
	}

	public boolean isDeliverBalls() {
		return deliverBalls;
	}

	public boolean isCollectInDangerZone() {
		return collectInDangerZone;
	}

	public boolean isMoveToGoal() {
		return moveToGoal;
	}
	
	public boolean isPoseRequested() {
		return poseRequested;
	}

	public boolean isUpdateCrossPosition() {
		return updateCrossPosition;
	}
	
	public boolean isPoseFailure() {
		return poseFailure;
	}

	public boolean isPoseReceived() {
		return poseReceived;
	}

	public boolean isHaveLinemap() {
		return haveLinemap;
	}

	public boolean isPoseUptoDate() {
		return poseUptoDate;
	}

	public boolean isRequstBall() {
		return requestBall;
	}

	public boolean isReadyToMoveToDestination() {
		return readyToMoveToDestination;
	}
	
	public boolean isReadyToStart() {
		return readyToStart;
	}

	public boolean isBallInDangerZone() {
		return ballInDangerZone;
	}

	public boolean isConnectionActive(){
		return activeConnection;
	}

	public boolean isUpdatedLinemap() {
		return updatedLinemap;
	}

	public boolean isBallLocationReceived() {
		return ballLocationReceived;
	}

	public boolean isNoMoreBalls() {
		return noMoreBalls;
	}

	public void setBallsDelivered(boolean ballsDelivered) {
		this.ballsDelivered = ballsDelivered;
	}

	public void setTrackwidth(double trackwidth) {
		this.trackwidth = trackwidth;
	}

	public void setCrossReceived(boolean crossReceived) {
		this.crossReceived = crossReceived;
	}

	public void setGoalLocation(Waypoint goalLocation) {
		this.goalLocation = goalLocation;
	}

	public void setDeliverBalls(boolean deliverBalls) {
		this.deliverBalls = deliverBalls;
	}

	public void setCollectInDangerZone(boolean collectInDangerZone) {
		this.collectInDangerZone = collectInDangerZone;
	}

	public void setDirectionFinder(GyroDirectionFinder directionFinder) {
		this.directionFinder = directionFinder;
	}

	public void setPoseRequested(boolean poseRequested) {
		this.poseRequested = poseRequested;
	}

	public void setUpdateCrossPosition(boolean updateCrossPosition) {
		this.updateCrossPosition = updateCrossPosition;
	}

	public void setPilot(DifferentialPilot pilot) {
		this.pilot = pilot;
	}

	public void setPoseReceived(boolean poseReceived) {
		this.poseReceived = poseReceived;
	}

	public void setReceivedPose(Pose receivedPose) {
		poseReceived = true;
		this.receivedPose = receivedPose;
	}

	public void setHaveLinemap(boolean haveLinemap) {
		this.haveLinemap = haveLinemap;
	}
	
	public void setPoseFailure(boolean poseFailure) {
		this.poseFailure = poseFailure;
	}

	public void setPoseUptoDate(boolean poseUptoDate) {
		this.poseUptoDate = poseUptoDate;
	}

	public void setRequestBall(boolean activeBall) {
		this.requestBall = activeBall;
	}
	
	public void setReadyToStart(boolean readyToStart) {
		this.readyToStart = readyToStart;
	}

	public void setCrossCenter(Waypoint crossCenter) {
		this.crossCenter = crossCenter;
	}

	public void setMax_X(double max_X) {
		this.max_X = max_X;
	}


	public void setUpdatedLinemap(boolean updatedLinemap) {
		this.updatedLinemap = updatedLinemap;
	}

	public void setReadyToMoveToDestination(boolean readyToMoveToDestination) {
		this.readyToMoveToDestination = readyToMoveToDestination;
	}


	public void setMax_Y(double max_Y) {
		this.max_Y = max_Y;
	}

	public void setDangerZone(double dangerZone) {
		this.dangerZone = dangerZone;
	}
	
	public void setEventTimer(Timer eventTimer) {
		this.eventTimer = eventTimer;
	}

	public void setCrossDangerZone(double crossDangerZone) {
		this.crossDangerZone = crossDangerZone;
	}

	public void setCurrentDestination(Waypoint currentDestination) {
		this.currentDestination = currentDestination;
	}

	public void setBallInDangerZone(boolean ballInDangerZone) {
		this.ballInDangerZone = ballInDangerZone;
	}

	public void setBallLocation(Waypoint ballLocation) {
		if(ballLocation == null) {
			noMoreBalls = true;
		}
		else {
			this.ballLocation = ballLocation;
			stateBallReceived();
		}
	}
	
	public void setBallInCross(boolean ballInCross) {
		this.ballInCross = ballInCross;
	}
	
	public void setConnectionFailure(boolean connectionFailure) {
		this.connectionFailure = connectionFailure;
	}

	public void setIsConnectionActive(boolean activeConnection) {
		this.activeConnection = activeConnection;
	}

	public void setLineMap(LineMap lineMap) {
		this.lineMap = lineMap;
		updatedLinemap = true;
	}

	public void setMoveToGoal(boolean moveToGoal) {
		this.moveToGoal = moveToGoal;
	}
	
	public void setPathFinder(ShortestPathFinder shortestPathFinder) {
		pathFinder = shortestPathFinder;
	}

	public void setNoMoreBalls(boolean noMoreBalls) {
		this.noMoreBalls = noMoreBalls;
	}


	public String toString() {
		return new StringBuilder()
				.append(this.getClass().getSimpleName()+"\n")
				.append("\t BOOLEAN STATE VARIABLES \n")
				.append("\t\t activeConnection: "+activeConnection+"\n")
				.append("\t\t ballInDangerZone: "+ballInDangerZone+"\n")
				.append("\t\t ballInCorner: "+ballInCorner+"\n")
				.append("\t\t ballInCross: "+ballInCross+"\n")
				.append("\t\t ballLocationReceived: "+ballLocationReceived+"\n")
				.append("\t\t crossReceived: "+crossReceived+"\n")
				.append("\t\t poseReceived: "+poseReceived+"\n")
				.append("\t\t updatedLinemap: "+updatedLinemap+"\n")
				.append("\t\t readyToMoveToDestination: "+readyToMoveToDestination+"\n")
				.append("\t\t requestBall: "+requestBall+"\n")
				.append("\t\t poseUptoDate: "+poseUptoDate+"\n")
				.append("\t\t poseRequested: "+poseRequested+"\n")
				.append("\t\t haveLinemap: "+haveLinemap+"\n")
				.append("\t\t noMoreBalls: "+noMoreBalls+"\n")
				.append("\t\t collectInDangerZone: "+collectInDangerZone+"\n")
				.append("\t\t deliverBalls: "+deliverBalls+"\n")
				.append("\t\t updatedCrossPosition: "+updateCrossPosition+"\n")
				.append("\t\t updateGoalLocation: "+updateGoalLocation+"\n")
				.append("\t DATA VARIABLES: \n")
				.append("\t\t ballLocation: ("
						+((ballLocation == null) ? "N/A" : ballLocation.getX())+", "
						+((ballLocation == null) ? "N/A" : ballLocation.getY())+", "
						+((ballLocation == null) ? "N/A" : ballLocation.getHeading())+")\n")
				.append("\t\t currentDestionation: ("
						+((currentDestination == null) ? "N/A" :currentDestination.getX())+", "
						+((currentDestination == null) ? "N/A" :currentDestination.getY())+", "
						+((currentDestination == null) ? "N/A" :currentDestination.getHeading())+")\n")
				.append("\t\tRobotpose: " + navigator.getPoseProvider().getPose().toString() + "\n")
				.append("\t\tGyroscope heading: " + directionFinder.getDegrees() + "\n")
				.append("\t\t crossCenter: ("+((crossCenter == null) ? "N/A" : crossCenter.getX())
						+", "+((crossCenter == null) ? "N/A" : crossCenter.getY())+", "+
						((crossCenter == null) ? "N/A" :crossCenter.getHeading())+")\n")
				.append("\t\t crossDangerZone: "+crossDangerZone+"\n")
				.append("\t\t dangerZone: "+dangerZone+"\n")
				.append("\t\t goalLocation: ("+((goalLocation == null) ? "N/A" : goalLocation.getX())
						+", "+((goalLocation == null) ? "N/A" :goalLocation.getY())
						+", "+((goalLocation == null) ? "N/A" :goalLocation.getHeading())+")\n")
				.append("\t\t linearAcceleration: "+linearAcceleration+"\n")
				.append("\t\t linearSpeed: "+linearSpeed+"\n")
				.append("\t\t lineMap: "+lineMap+"\n")
				.append("\t\t (max_X, max_Y): ("+max_X+", "+max_Y+")\n")
				.append("\t\t motorA: "+motorA+"\n")
				.append("\t\t motorB: "+motorB+"\n")
				.append("\t\t motorC: "+motorC+"\n")
				.append("\t\t motorD: "+motorD+"\n")
				.append("\t\t navigator: "+navigator+"\n")
				.append("\t\t pathFinder: "+pathFinder+"\n")
				.append("\t\t pilot: "+pilot+"\n")
				.append("\t\t receivedPose: "+receivedPose+"\n")
				.append("\t\t trackWidth: "+trackwidth+"\n")
				.append("\t\t wheelDiametere: "+wheelDiameter+"\n")
				.append("\t\t receivedPose: "+receivedPose+"\n")
				.append("\t INFORMATION FROM PILOT AND NAVIGATOR: \n")
				.append("\t\t Angularacceleration Speed: "+pilot.getAngularAcceleration()+"\n")
				.append("\t\t Linearacceleration Speed: "+pilot.getLinearAcceleration()+"\n")
				.append("\t\t Max Linearspeed: "+pilot.getMaxLinearSpeed()+"\n")
				.append("\t\t Max Angularspeed: "+pilot.getMaxAngularSpeed()+"\n")
				.append("\t\t robotPose: ("+navigator.getPoseProvider().getPose().getX()
						+", "+navigator.getPoseProvider().getPose().getY()
						+", "+navigator.getPoseProvider().getPose().getHeading()+")\n")
				.toString();
	}

	public void setUpdateGoalLocation(boolean updateGoalLocation) {
		this.updateGoalLocation = updateGoalLocation;

	}

	public boolean isUpdateGoalLocation() {
		return updateGoalLocation;
	}

	public void setBallInCorner(boolean b) {
		ballInCorner = b;
	}

	public boolean getBallInCorner() {
		return ballInCorner;
	}

	public long getTimeStampStop() {
		return timeStampStop;
	}
	
	public Timer getEventTimer() {
		return eventTimer;
	}

	public void setTimeStampStop(long timeStampStop) {
		this.timeStampStop = timeStampStop;
	}

	public long getTimeStampStart() {
		return timeStampStart;
	}

	public void setTimeStampStart(long timeStampStart) {
		this.timeStampStart = timeStampStart;
	}

	public void setTimeIsUp(boolean timeIsUp) {
		this.timeIsUp=timeIsUp;
	}
	
	public boolean isTimeIsUp() {
		return timeIsUp;
	}
	
	/**
	 * Corrects the heading
	 */
	public void correctHeading(Pose pose1, Waypoint waypoint1, double borderValue) {
		// Calculating angle
		double x1, y1, x2, y2;
		x1 = pose1.getX();
		y1 = pose1.getY();
		x2 = waypoint1.getX();
		y2 = waypoint1.getY();
		
		double deltaX = x2-x1;
		double deltaY = y2-y1;
		
		double heading = Math.toDegrees(Math.atan2(deltaY,deltaX));
		
		System.out.println("Heading coor: " + heading);
		
		if(Math.abs(heading)>borderValue)
			navigator.rotateTo(heading);
	}
}
