package utility;

import data.RobotState;
import lejos.utility.TimerListener;

public class EventTimerListener implements TimerListener{
	RobotState robotState = RobotState.getInstance();
	@Override
	public void timedOut() {
		robotState.setTimeIsUp(true);
		robotState.setMoveToGoal(true);
	}
	
}