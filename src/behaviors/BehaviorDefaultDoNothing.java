package behaviors;

import lejos.robotics.subsumption.Behavior;

public class BehaviorDefaultDoNothing implements Behavior {
	private boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** DO NOTHING BEHAVIOR ***");

		while(!suppressed)
			Thread.yield();
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
}
