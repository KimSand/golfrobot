package behaviors;

import data.RobotState;
import lejos.robotics.RangeFinderAdapter;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.CompassPilot;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Pose;
import lejos.robotics.subsumption.Behavior;


@SuppressWarnings("deprecation")
public class BehaviorCarefulCollectionSensor implements Behavior{
	RobotState robotState = RobotState.getInstance();
	
	Pose robotPoseBefore = robotState.getNavigator().getPoseProvider().getPose();
	DifferentialPilot pilot = robotState.getPilot();
	RangeFinderAdapter rf = robotState.getRf();
	
	RegulatedMotor leftMotor = robotState.getMotorB();
	RegulatedMotor rightMotor = robotState.getMotorC();
	
	boolean suppressed = true;
	private int offSet = 0; // Degrees to rotate extra, to catch the ball.
	private int offSetCross = 5;
	private int offSetCorner = 80;
	private double distance = 200; // Object to sensor
	private double correctHeadingBorder = 2;
		
	@Override
	public boolean takeControl() {		
		return robotState.isCollectInDangerZone() && robotState.isPoseUptoDate();
	}

	@Override
	public void action() {
		/* States, do not change */
		suppressed = false;
		System.out.println("*** BHEAVIOR CARFULCOLLECTION ***");
				
		if(robotState.isDebug()) {
			System.out.println(robotState);	
		}
		/* States block stops */
		
		robotState.correctHeading(robotState.getNavigator().getPoseProvider().getPose(),
				robotState.getBallLocation(),
				correctHeadingBorder);
		
		int tachoCountStart = robotState.getMotorB().getTachoCount();
		System.out.println("TachoCountStart: " + tachoCountStart);
		
		setMotorSpeed(275); // Is this appropriate?
		
		setMotorDirection(true); // FORWARD --->
		
		
		// Checking that we do not hit the edge :(
		double range = rf.getRange();
		while(range > distance) {
			if(robotState.isDebug()) {
				System.out.println("Distance: " + range);
			}
			range = rf.getRange();	
		}
		if(robotState.isDebug()) {
			System.out.println("Distance: " + range);
		}
		
		stopMotors();
		int tachoCountStop = robotState.getMotorB().getTachoCount();
		System.out.println("TachoCountStop: " + tachoCountStop);
		System.out.println("Tacho diff: " + (tachoCountStop-tachoCountStart));
		
		if(robotState.isBallInCross()) {
			rotateMotors(offSetCross, true);
			wiggle(15);
		}
		else if (robotState.getBallInCorner()){
			rotateMotors(offSetCorner, true);
		}
		else {
			rotateMotors(offSet, true);
			wiggle(30);			
		} 
		
		rotateMotors(-420, true);
		
		/* States, do not change */
		robotState.setPoseUptoDate(false);
		robotState.stateRequestBall();
}

	@Override
	public void suppress() {
		suppressed = true;
	}
	
	private void rotateMotors(int degrees, boolean block) {
		leftMotor.rotate(degrees, true);
		rightMotor.rotate(degrees, !block);
	}

	private void wiggle(int wiggleSize) {
		robotState.getPilot().rotate(wiggleSize, false);
		robotState.getPilot().rotate(-2*wiggleSize, false);
		robotState.getPilot().rotate(wiggleSize, false);
	}
	
	private void setMotorDirection(boolean forward) {
		if(forward) {
			leftMotor.forward();
			rightMotor.forward();
		} else {
			rightMotor.backward();
			leftMotor.backward();
		}
	}
	
	private void setMotorSpeed(int speed) {
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(speed);
	}
	
	private void stopMotors() {
		leftMotor.stop(true);
		rightMotor.stop(true);		
	}
}
