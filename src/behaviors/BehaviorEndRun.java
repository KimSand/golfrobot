package behaviors;

import data.RobotState;
import lejos.robotics.subsumption.Behavior;

public class BehaviorEndRun implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return (robotState.isTimeIsUp() 
				&& robotState.isBallsDelivered())
				||(robotState.isNoMoreBalls() 
				&& robotState.isBallsDelivered());
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR SHUTDOWN ***");
		robotState.setTimeStampStop(System.currentTimeMillis());
		System.out.println(formatedTime(robotState.getTimeStampStop()-robotState.getTimeStampStart()));

		System.exit(0);
	}

	private String formatedTime(long l) {
		return String.format("%02d min,  %02d sec", (l/(1000*60))%60 ,(l/1000)%60);
	}

	@Override
	public void suppress() {
		suppressed = true;
	}

}
