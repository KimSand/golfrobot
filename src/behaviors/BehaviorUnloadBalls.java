package behaviors;

import data.RobotState;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.geometry.Point;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.subsumption.Behavior;


/**
 * TODO Javadocs
 * <p>
 * Current scope:
 * Handles delivering balls through goal gap once the robot has moved to the delivering point.
 * <p>
 * Need access to:
 * Navigator for current position/heading, deliver point, balls collected/balls remaining data/checks.
 * @author Hvalfisk
 * 
 *
 */
public class BehaviorUnloadBalls implements Behavior{
	boolean suppressed = false;
	
	Point robotLocation;
	Point deliverLocation;
	
	RobotState robotState = RobotState.getInstance();
	final float MARGIN_OF_ERROR_Y = 0.5f;
	final float MARGIN_OF_ERROR_X = 1.0f;
	
	
	RegulatedMotor toungeMotor = robotState.getMotorD();
	RegulatedMotor wheelsMotor = robotState.getMotorA();
	RegulatedMotor left = robotState.getMotorB();
	RegulatedMotor right = robotState.getMotorC();

	
	Navigator navigator = robotState.getNavigator();
	
	
	/**
	 * Check variables from TrackData: ballCounter, deliverPoint.
	 * <p>
	 * Handles balls collected, position deviation.
	 */
	@Override
	public boolean takeControl() {
		return  (robotState.isTimeIsUp()
				&& robotState.isDeliverBalls())
				||(robotState.isDeliverBalls() 
				&& robotState.isPoseFailure())
				|| (robotState.isDeliverBalls()
				&& robotState.isConnectionFailure())
				|| (robotState.isNoMoreBalls() 
				&& robotState.isDeliverBalls()
				);
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR UNLOAD BALLS ***");

		if(robotState.isDebug()) {
			System.out.println(robotState);	
		}
		
		//Deliver balls
		this.wheelsMotor.setSpeed(1600);
		
		// Start tounge
		this.toungeMotor.setSpeed(1600);
		this.toungeMotor.forward();
		
		//pulse the wheels 10 times.
		//this.wheelsMotor.backward();
		for (int pulse = 0; pulse < 10;pulse++) {
			this.wheelsMotor.rotate(120);//FIND UD AF HVOR MEGET DEN SKAL ROTERE FOR AT SKYDE EN BOLD UD.
//			Delay.msDelay(200); // do nothing
		}
		
		//SHAKE YOUR BOOTY
		this.wheelsMotor.forward();
		this.left.setSpeed(400);
		this.right.setSpeed(400);
		
		left.rotate(-60,true);
		right.rotate(-60,true);
		left.waitComplete();
		right.waitComplete();
		left.rotate(60,true);
		right.rotate(60,true);
		left.waitComplete();
		right.waitComplete();
		left.rotate(-60,true);
		right.rotate(-60,true);;
		left.waitComplete();
		right.waitComplete();
		left.rotate(60,true);
		right.rotate(60,true);

				
		wheelsMotor.stop();
		toungeMotor.stop();
		
		rotateMotors(-420, false);
		
		robotState.stateBallsDelivered();
	}


	@Override
	public void suppress() {
		suppressed = true;
	}
	
	
	private void rotateMotors(int degrees, boolean block) {
		left.rotate(degrees, true);
		right.rotate(degrees, !block);
	}

}