package behaviors;

import data.RobotState;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.CompassPilot;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Behavior;

public class BehaviorCalibrate implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		DifferentialPilot pilot = robotState.getPilot();
		
		
		RegulatedMotor motor = robotState.getMotorA();
		motor.setSpeed((int) motor.getMaxSpeed());
		motor.backward();

		RegulatedMotor motor1 = robotState.getMotorD();
		motor1.setSpeed((int) motor1.getMaxSpeed());
		motor1.forward(); 
		// Travel 1 meter	
		pilot.travel(100);
		
		// Rotate 3 laps
//		pilot.rotate(1080);
		
		System.exit(0);
		
	}

	@Override
	public void suppress() {
		//lulz
	}
}
