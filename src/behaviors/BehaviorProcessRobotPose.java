package behaviors;

import data.RobotState;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.subsumption.Behavior;

public class BehaviorProcessRobotPose implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	private boolean updatingPose = false;
	private Pose lastKnownPose;
	private int poseRequestAttempts = 1;
	private int poseRequestThreshold = 3;
	private boolean noPose = false;

	@Override
	public boolean takeControl() {
		return updatingPose 
				|| robotState.isPoseReceived() 
				&& !robotState.isPoseUptoDate();
	}

	@Override
	public void action() { 
		suppressed = false;
		updatingPose = true;

		System.out.println("*** BEHAVIOR PROCESS POSE ***");
		if(robotState.isDebug()) {
			System.out.println(robotState);
		}

		if(robotState.getReceivedPose() == null) {
			if(poseRequestAttempts++ % poseRequestThreshold == 0) {
				Path path;
				if(robotState.isPoseFailure()) {
					robotState.stateExit();
				}
				else{
					robotState.setMoveToGoal(true);
					robotState.setPoseFailure(true);
				}
			}
			else {
				robotState.stateRequestPose();
			}	
		}
		else {
			poseRequestAttempts = 1;
			lastKnownPose = robotState.getReceivedPose();
			robotState.getNavigator().getPoseProvider().setPose(robotState.getReceivedPose());
			robotState.getDirectionFinder().setDegrees(robotState.getReceivedPose().getHeading());
			robotState.setPoseUptoDate(true);
			robotState.setPoseFailure(false);
			robotState.setMoveToGoal(false);
		}

		robotState.setPoseReceived(false);
		updatingPose = false;
	}

	@Override
	public void suppress() {
		suppressed = true;	
		updatingPose = false;
	}

}
