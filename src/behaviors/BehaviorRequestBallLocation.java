package behaviors;

import java.io.IOException;

import communication.Communicator;
import communication.outgoingMessages;
import data.RobotState;
import lejos.robotics.subsumption.Behavior;

public class BehaviorRequestBallLocation implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return robotState.isRequstBall() && robotState.isHaveLinemap();
	}

	@Override
	public void action() {
		suppressed = false;
		Communicator communicator;
		System.out.println("*** BEHAVIOR REQUEST BALL LOCATION ***");
		if(robotState.isDebug()) {

			System.out.println(robotState);	
		}

		robotState.setRequestBall(false);
		
		try {
			communicator = Communicator.getInstance();
			communicator.sendData(outgoingMessages.RequestBallLocation);
		} catch (IOException e) {
			System.out.println("ERROR: COULDN'T FETCH COMMUNICATOR INSTANCE.");
			System.exit(0);
		}

	}

	@Override
	public void suppress() {
		suppressed = true;
	}

}
