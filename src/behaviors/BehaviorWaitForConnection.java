package behaviors;

import java.io.IOException;

import communication.Communicator;
import data.RobotState;
import lejos.hardware.Sound;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Timer;
import lejos.utility.TimerListener;

public class BehaviorWaitForConnection implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return !robotState.isConnectionActive();
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR WAIT FOR CONNECTION ***");
		
		try {
			Communicator.getInstance().createConnection();
			
			if(!Communicator.getInstance().isAlive())
				Communicator.getInstance().start();
			
			robotState.setIsConnectionActive(true);
			robotState.setConnectionFailure(false);
			
		} catch (IOException e) {
			System.out.println("ERROR: FAILED TO CREATE A CONNECTION.");
			e.printStackTrace();
			System.out.println(0);
		}
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
	


}
