package behaviors;


import data.RobotState;
import lejos.hardware.BrickFinder;
import lejos.hardware.Sound;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.Motor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.geometry.Line;
import lejos.robotics.navigation.CompassPilot;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

public class BehaviorMoveToBall implements Behavior {
	private RobotState robotState = RobotState.getInstance();
	boolean suppressed = true;
	private int noRuns = 0;
	private int callibrateThreshold = 5;
	

	@Override
	public boolean takeControl() {
		return robotState.isReadyToMoveToDestination();
	}

	@Override
	public void action() {
		System.out.println("*** MOVE TO BALL BEHAVIOR ***");
		suppressed = false;
		if(robotState.isDebug()) {
			System.out.println(robotState);	
		}
		
		//TODO: MOTOR FUNCTIONS SHOULD BE MOVED
		RegulatedMotor motor = Motor.A;
		motor.setSpeed(1600);
		motor.backward();

		RegulatedMotor motor1 = Motor.D;
		motor1.setSpeed(1600);
		motor1.backward(); 
		
		moveToPoint();

		while(robotState.getNavigator().isMoving() && !suppressed)
			Thread.yield();

		if(robotState.isBallInDangerZone()) {
			robotState.getNavigator().rotateTo(robotState.getCurrentDestination().getHeading());
			robotState.setCollectInDangerZone(true);
		}
		else {
			robotState.stateRequestBall();
		}
		
		Delay.msDelay(100); // Ensures that we pick up the bold, before requesting the Robot Pose
		robotState.stateRequestPose();
		noRuns++;
	}

	@Override
	public void suppress() {
		suppressed = true;
	}

	/**
	 * Tænder for opsamlingsmekanismen, og kører til activeWaypoint 
	 * defineret i robotState ved hjælp af shortest path finder.
	 */
	private void moveToPoint() {
		try {
			Path path = robotState.getPathFinder().findRoute(
					robotState.getNavigator().getPoseProvider().getPose(),
					robotState.getCurrentDestination()
					);
			
			drawToScreen(path);
			robotState.getNavigator().followPath(path);
		} catch (DestinationUnreachableException e) {
			System.out.println("Destination unreachable");
		}
	}
	
	private void drawToScreen(Path path) {
		GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();
		g.clear();
		for(Line line : robotState.getLineMap().getLines())
			g.drawLine((int) line.x1, (int)line.y1, (int)line.x2, (int)line.y2);
			
		for(int i = 1; i<path.size(); i++) {
			Line line = new Line(path.get(i-1).x, path.get(i-1).y, path.get(i).x, path.get(i).y);
			g.drawLine((int)line.x1, (int)line.y1, (int)line.x2, (int)line.y2);
		}
	}
}
