package communication;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

import javax.xml.stream.XMLStreamException;

import data.RobotState;
import lejos.hardware.Button;
import lejos.robotics.mapping.LineMap;
import lejos.robotics.mapping.SVGMapLoader;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;


public class Communicator extends Thread implements Runnable{
	private static Communicator instance = null;
	private Connection connection;
	private DataParser dataParser;
	private LinkedList<String> dataBufferList = new LinkedList<String>();
	private RobotState robotState = RobotState.getInstance();
	private Navigator navigator;

	public static Communicator getInstance() throws IOException {
		if (instance == null)
			instance = new Communicator();

		return instance;
	}

	private Communicator() throws IOException {
		connection = new Connection();
		dataParser = new DataParser();
	}

	public void createConnection() throws IOException {

		connection.establishIncommingConnection();
		connection.establishOutgoingConnection();
		
		System.out.println("\t Communicator: Connection established");
	}

	@Override
	public void run() {
		while (true) {
			if(robotState.isConnectionActive()) {
				try {
					String tempString = connection.readData();
					switch (messageLimiters.valueOf(tempString)) {
					case MAPSTART:
						robotState.stateStartRecvLinemap();
						readDataIntoBuffer(messageLimiters.MAPEND);
						robotState.setLineMap(dataParser.parseLineMap());
						break;
					case BALLSSTART:
						readDataIntoBuffer(messageLimiters.BALLSEND);
						try {
							robotState.setBallLocation(dataParser.parseBallLocation());
						} catch (Exception e) {
							System.out.println("ERROR: COULDN'T PARSE BALL POSITION!");
							System.out.println("\t message: "+e.getMessage());
							System.out.println("\t cause: "+e.getCause());
						}
						break;
					case POSESTART:
						readDataIntoBuffer(messageLimiters.POSEEND);
						try {
							robotState.setReceivedPose(dataParser.parseRobotPose());
							robotState.setPoseRequested(false);
						} catch (Exception e) {
							System.out.println("ERROR: COULDN'T PARSE ROBOT POSE!");
							System.out.println("\t message: "+e.getMessage());
							System.out.println("\t cause: "+e.getCause());
						}
						break;
					case CROSSSTART:
						readDataIntoBuffer(messageLimiters.CROSSEND);
						try {
							Waypoint crossLocation = dataParser.parseCrossLocation();
							if(crossLocation != null) {
								robotState.setCrossCenter(crossLocation);
								robotState.setCrossReceived(true);
							}
							robotState.setUpdateCrossPosition(false);
						} catch (Exception e) {
							System.out.println("ERROR: COULDN'T PARSE CROSS LOCATION!");
							System.out.println("\t message: "+e.getMessage());
							System.out.println("\t cause: "+e.getCause());
						}
						break;
					case GOALSTART:
						readDataIntoBuffer(messageLimiters.GOALEND);
						try {
							Waypoint goalLocation = dataParser.parseGoalLocation();
							if(goalLocation != null) {
								robotState.setGoalLocation(goalLocation);
							}
							robotState.setUpdateGoalLocation(false);
						} catch (Exception e) {
							System.out.println("ERROR: COULDN'T PARSE GOAL LOCATION!");
							System.out.println("\t message: "+e.getMessage());
							System.out.println("\t cause: "+e.getCause());
						}
						break;
					case ABORT:
						System.out.println("AAA");
						break;
					default:
						break;
					}
				} catch (IllegalArgumentException e) {
					System.out.println("ERROR: RECEIVED ILLEGAL ARGUMENT!");
				} catch (IOException e) {
					if(!robotState.isConnectionFailure()) {
						robotState.setIsConnectionActive(false);
						robotState.setConnectionFailure(true);
						robotState.setMoveToGoal(true);
					}

				} finally {
					dataBufferList = new LinkedList<String>();
				}
			}
			else {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void readDataIntoBuffer(messageLimiters limiterToLookFor) throws IOException {
		while(true) {
			String tempString = connection.readData();
			if(tempString.equals(limiterToLookFor.name())) {
				return;
			}
			dataBufferList.add(tempString);
		}
	}

	public void sendData(outgoingMessages message) {
		switch(message) {
		case RequestBallLocation:
			try {
				connection.sendData("BALL");
			} catch (IOException e) {
				System.out.println("\t "+getClass().getSimpleName()+"\n"
						+"\t ERROR: COULDN'T SEND DATA.");
			}
			break;
		case RequestRobotPose:
			try {
				connection.sendData("POSE");
			} catch (IOException e) {
				System.out.println("\t "+getClass().getSimpleName()+"\n"
						+"\t ERROR: COULDN'T SEND DATA.");
			}
			break;

		}
	}


	private class Connection implements IServerCommunication{
		private final int SERVER_IN_PORT = 1235;
		private final int SERVER_OUT_PORT = 1234;
		private ServerSocket serverIn;
		private ServerSocket serverOut;
		private Socket incommingConnectionSocket;
		private Socket outgoingConnectionSocket;
		private DataInputStream inFromClient;
		private DataOutputStream dataOut;

		public Connection() throws IOException{
			serverIn = new ServerSocket(SERVER_IN_PORT);
			serverOut = new ServerSocket(SERVER_OUT_PORT);
			serverOut.setSoTimeout(5000); 
		}

		@Override
		public void establishIncommingConnection() throws IOException {
			System.out.println("\t"+getClass().getSimpleName()+": ESTABLISH INCOMMING CONNECTION");
			incommingConnectionSocket = serverIn.accept();
			inFromClient = new DataInputStream(
					incommingConnectionSocket.getInputStream());	
		}

		@Override
		public void establishOutgoingConnection() throws IOException {
			System.out.println("\t"+getClass().getSimpleName()+": ESTABLISH OUTGOING CONNECTION");
			outgoingConnectionSocket = serverOut.accept();
			dataOut = new DataOutputStream(
					outgoingConnectionSocket.getOutputStream());	

		}

		@Override
		public String readData() throws EOFException, IOException {
			StringBuilder stringBuilder = new StringBuilder();
			boolean rReceived = false;
			try {
				while (true) {
					byte readByte = inFromClient.readByte();
					byte[] read = {readByte};

					// This is perhaps a having calculation
					if (rReceived) {
						if (read[0] == (byte) 10) { // byte 10 -> \n
							break;
						}
					}
					if (read[0] == (byte) 13) { // byte 13 -> \r
						rReceived = true;
						continue;
					}
					stringBuilder.append(new String(read, StandardCharsets.UTF_8));

				}
				System.out.println("\t"+getClass().getSimpleName()+" readData(): "+stringBuilder.toString());
				return stringBuilder.toString();
			} catch(EOFException e) {
				closeConnection();
				throw new EOFException("ERROR: CONNECTION LOST");
			} catch (IOException e) {
				throw new IOException("ERROR: FAILED TO READ DATA");
			}
		}

		@Override
		public void closeConnection() throws IOException {
			inFromClient.close();
			incommingConnectionSocket.close();

			outgoingConnectionSocket.close();
			dataOut.close();
		}

		@Override
		public void sendData(String data) throws IOException {
			dataOut.writeUTF(data);
			dataOut.flush();
		}
	}

	private class DataParser implements IDataParser {
		@Override
		public Waypoint parseBallLocation() throws Exception {
			StringBuilder builder = new StringBuilder();
			for(String str: dataBufferList) {
				builder.append(str);
			}

			if(dataBufferList.size() != 1) {
				throw new Exception("Received more than one ball position in parseBalls()");
			}else {
				String[] coords = dataBufferList.get(0).split(",");
				if(coords.length == 2) {
					return new Waypoint(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));
				} else if(coords.length == 1) {
					return null;
				} else {
					throw new Exception("Eror while splitting coordinates in parseBalls()");
				}
			}
		}

		public Waypoint parseGoalLocation() throws Exception {
			StringBuilder builder = new StringBuilder();

			for(String str: dataBufferList) {
				builder.append(str);
			}

			if(dataBufferList.size() != 1) {
				throw new Exception("Received more than one goal location position in parseCrossLocation()");
			}else {
				String[] coords = dataBufferList.get(0).split(",");
				System.out.println("cross location coords length: "+coords.length);
				if(coords.length == 2) {
					return new Waypoint(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));
				} else if(coords.length == 1) {
					return null;
				} else {
					throw new Exception("Error while splitting coordinates in parseGoalLocation()");
				}
			}
		}

		@Override
		public LineMap parseLineMap() {
			System.out.println("\t "+getClass().getSimpleName());
			System.out.println("\t\tlength Parsing linemap.");
			StringBuilder builder = new StringBuilder();
			SVGMapLoader svgml = null;
			LineMap map = null;

			for(String str: dataBufferList) {
				builder.append(str);
			}
			InputStream stream = new ByteArrayInputStream(builder.toString().getBytes());

			try {
				System.out.println("\t\t SVGMapLoader started.");
				svgml = new SVGMapLoader(stream);
				System.out.println("\t\t SVGMapLoader readLineMap().");
				map = svgml.readLineMap();
			} catch (XMLStreamException e) {
				System.out.println("SVGML readline failed");
				while(!Button.ENTER.isDown());
			}
			System.out.println("\t\t Done parsing linemap.");
			return map;
		}

		@Override
		public Pose parseRobotPose() throws Exception {
			System.out.println("\t"+getClass().getSimpleName());

			StringBuilder builder = new StringBuilder();
			for(String str: dataBufferList) {
				builder.append(str);
			}

			if(dataBufferList.size() != 1) {
				throw new Exception("\t\t Received more than one robot position");
			}else {
				String[] coords = dataBufferList.get(0).split(",");
				if(coords.length == 3) {
					return new Pose(Float.parseFloat(coords[0]), 
							Float.parseFloat(coords[1]), 
							Float.parseFloat(coords[2])
							);
				} else if(coords.length == 1) {
					return null;
				} else {
					throw new Exception("Eror while splitting coordinates in parseRobotPose()");
				}
			}
		}

		@Override
		public Waypoint parseCrossLocation() throws Exception {
			StringBuilder builder = new StringBuilder();
			for(String str: dataBufferList) {
				builder.append(str);
			}

			if(dataBufferList.size() != 1) {
				throw new Exception("Received more than one cross position in parseCrossLocation()");
			}else {
				String[] coords = dataBufferList.get(0).split(",");
				if(coords.length == 2) {
					return new Waypoint(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));
				} else if(coords.length == 1) {
					return null;
				} else {
					throw new Exception("Eror while splitting coordinates in parseCrossLocation()");
				}
			}
		}
	}
}
