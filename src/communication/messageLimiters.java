package communication;

public enum messageLimiters {
	MAPSTART,
	MAPEND,
	BALLSSTART,
	BALLSEND,
	POSESTART,
	POSEEND,
	CROSSSTART,
	CROSSEND,
	GOALSTART,
	GOALEND,
	ABORT;
	
}

