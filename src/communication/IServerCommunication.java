package communication;

import java.io.EOFException;
import java.io.IOException;

public interface IServerCommunication {
	public void establishIncommingConnection() throws IOException;
	public void establishOutgoingConnection() throws IOException;
	public String readData() throws EOFException, IOException;
	public void sendData(String data) throws IOException;
	public void closeConnection() throws IOException;
}
