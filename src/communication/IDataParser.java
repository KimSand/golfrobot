package communication;

import lejos.robotics.mapping.LineMap;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;

public interface IDataParser {
	public Waypoint parseBallLocation() throws Exception;
	public LineMap parseLineMap();
	public Pose parseRobotPose() throws Exception;
	public Waypoint parseCrossLocation() throws Exception;
}
